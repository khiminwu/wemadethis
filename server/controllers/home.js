module.exports.controller = function(app) {

/**
 * a home page route
 */
  var io = require('../io');
		var MobileDetect = require('mobile-detect');
		var socketCodes = [];
  
  io.sockets.on('connection', function(socket){
	 socket.emit('welcome');

     socket.on("device", function(device){
     	  if(device.type == "game")
	      {
	      	
	         console.log('game:'+device.gameCode);
	         // Store game code -> socket association
	         var sock = [];
	         sock['code'] = device.gameCode;
	         sock['game'] = io.sockets.connected[socket.id];
	         sock['controller'] = false;
	         //socketCodes[gameCode] = sock;
	         socketCodes.push(sock);
	         //console.log(socketCodes);
	         //console.log(io.sockets.connected[socket.id])
	         socket.gameCode = device.gameCode;
	         
	      }else if(device.type == "controller"){
	         // if game code is valid...
	         //console.log('controller:'+device.gameCode);
	         var sock = checkArrayByCode(device.gameCode);
	        // console.log(sock,'sadasd');
	         if(sock){
	         	if(sock['controller']==false){
		            // save the game code for controller commands
		            socket.gameCode = device.gameCode

		            // initialize the controller
		            
		            sock['controller'] = io.sockets.connected[socket.id];
		            sock['controller'].emit("connected", {});
		            // start the game
		            //console.log(sock['game']);
		            //sock['game'].emit("connected");
	         	}else{
	         		socket.emit("fail", {});
	            	socket.disconnect();
	         	}
	         }else{
	            socket.emit("fail", {});
	            socket.disconnect();
	         }
	      }

     });

		/*SHOW CTA NEXT SCENE*/

		socket.on('showNextBtn',function(data){
			var sock = checkArrayByCode(data.gameCode);
			sock['controller'].emit("showNextBtn",{'page':data.page})
	        //sock['game'].emit("nextPage",{'page':data.page});
		})


		socket.on('startJourney', function (data) {
			var sock = checkArrayByCode(data.gameCode);
	        sock['game'].emit("connected");
	    });

		/*SHOW CTA NEXT SCENE*/

		socket.on('showNextPage',function(data){
			var sock = checkArrayByCode(data.gameCode);
	        sock['game'].emit("showNextPage",{'page':data.page});
		})		
		
		/*KNOCKING DOOR*/
		
		socket.on('knockSuccess',function(data){
			var sock = checkArrayByCode(data.gameCode);
	        sock['game'].emit("knockSuccess");
		})

		/*KNOCKING DOOR*/


		/*SWIPE SMOKE*/
		
		socket.on('startSwipeGame', function (data) {
			var sock = checkArrayByCode(data.gameCode);
	        sock['game'].emit("startSwipeGame");
	    });
		socket.on('swipeStart', function (data) {
			var sock = checkArrayByCode(data.gameCode);
	        sock['game'].emit("swipeStart");
	    });

	    socket.on('swipeEnd', function (data) {
			var sock = checkArrayByCode(data.gameCode);
	        sock['game'].emit("swipeEnd");
	    });
	    /*SWIPE SMOKE*/


		 
		socket.on('startGame', function (data) {
			console.log('play',data.gameCode);
			var sock = checkArrayByCode(data.gameCode);
	        sock['game'].emit("playing", {});
	    });

	    socket.on('countdown', function (data) {
			console.log(data.count);
			var sock = checkArrayByCode(data.gameCode);
	        sock['controller'].emit("countdown", {count:data.count});
	    });

	    socket.on('startPlaying', function (data) {
			var sock = checkArrayByCode(data.gameCode);
	        sock['controller'].emit("startPlaying", {});
	    });

	    socket.on('gameOver', function (data) {
			console.log('game over');
			var sock = checkArrayByCode(data.gameCode);
	        sock['controller'].emit("gameOver", {score:data.score});
	    });


	    socket.on('disconnect', function () {
		 	
	        	var result = checkArrayGame(socket.id);

	        	if(!result){
	        		result = checkArrayController(socket.id);
	        	}

	        	
	        	if(result){
	        		if(result['device']=='game'){
	        			if(result['socket']['controller']){
		        			result['socket']['controller'].emit('connectionLost',{})
		        		}
	        			socketCodes.splice(result['device'],1)
	        		}else if(result['device']=='controller'){
	        			//console.log('asdsad'+result['socket']['controller']);
		        		result['socket']['game'].emit('connectionLost',{})
		        		socketCodes[result['index']]['controller']=false;
	        			//result['socket']['controller']=false;
	        		}
	        	}
	    });

	    socket.on('swipe', function (device) {
			console.log(device.direction);
			var sock = checkArrayByCode(device.gameCode);
	        sock['game'].emit("checkDirection", {direction:device.direction});
	    });

		

	});

	function checkArrayByCode(gamecode){
		//console.log(gamecode);
		for(var i=0;i<socketCodes.length;i++){
			console.log(socketCodes[i]['code']);
			if(socketCodes[i]['code']==gamecode){
				return socketCodes[i];
			}
		
		}
	}

	function checkArrayGame(socket_id){
		for(var i=0;i<socketCodes.length;i++){
			if(socketCodes[i]['game'].id==socket_id){
				return {'device':'game','socket':socketCodes[i],'index':i}
			}
		}
	}

	function checkArrayController(socket_id){
		for(var i=0;i<socketCodes.length;i++){
			if(socketCodes[i]['controller'].id==socket_id){
				return {'device':'controller','socket':socketCodes[i],'index':i}
			}
		}
	}

  app.post('/home', function(req, res, next) {
  		var is_ajax_request = req.xhr;
  		if(is_ajax_request){
  			var base_url = req.protocol + '://' +req.get('host');
  			//res.send();
  			res.render('page/home',{URLs:base_url,deviceCode:req.body.device});
  		}
  });

  app.post('/house', function(req, res, next) {
  		var is_ajax_request = req.xhr;
  		if(is_ajax_request){
  			var base_url = req.protocol + '://' +req.get('host');
  			var sock = checkArrayByCode(req.body.device);
  			
	        if(sock && sock['controller'] && sock['game']){
	  			res.render('page/house',{URLs:base_url,deviceCode:req.body.device});
	  		}else{
	  			res.send('false');
	  		}

  		}
  });

  app.post('/door', function(req, res, next) {
  		var is_ajax_request = req.xhr;
  		if(is_ajax_request){
  			var base_url = req.protocol + '://' +req.get('host');
  			var sock = checkArrayByCode(req.body.device);
  			
	        if(sock && sock['controller'] && sock['game']){
	  			res.render('page/door',{URLs:base_url,deviceCode:req.body.device});
	  			sock['controller'].emit("knockArea");
	  		}else{
	  			res.send('false');
	  		}
  		}
  });

  app.post('/room', function(req, res, next) {
  		var is_ajax_request = req.xhr;
  		if(is_ajax_request){
  			var base_url = req.protocol + '://' +req.get('host');
  			var sock = checkArrayByCode(req.body.device);
  			
	        if(sock && sock['controller'] && sock['game']){
	  			res.render('page/room',{URLs:base_url,deviceCode:req.body.device});
	  			sock['controller'].emit("roomArea");
	  		}else{
	  			res.send('false');
	  		}
  		}
  });

  app.get('*', function(req, res, next) {
  		//res.render('mobile');
  		var base_url = req.protocol + '://' +req.get('host');
	  	var md = new MobileDetect(req.headers['user-agent']);
		if(md.mobile()){
			res.render('control/mobile', { title: 'Controller',deviceCode:req.query.code,URLs:base_url });
		}else{
			var crypto = require('crypto');
  			var gameCode = crypto.randomBytes(3).toString('hex');
  			gameCode = '1234';
	        res.render('index', { title: 'Home',URLs:base_url,deviceCode:gameCode });
		}
  });



}
