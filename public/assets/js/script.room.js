var swipe_success=0;

$(document).ready(function(){
	var home_scene = stage.getChildByName('home');
	stage.setChildIndex(home_scene,0);
	
	socket.on('startSwipeGame', function(data)
     {
     	var room_scene = stage.getChildByName('room');
     	var scale = 1.5;
		var posX = 0-((w*scale)-w)/2;
		var posY = 0-((h*scale)-h)/2;
		createjs.Tween.get(room_scene,{paused:false}).to({scaleX:scale,scaleY:scale,x:posX,y:posY},2000);
		$('#swipe-instruction1').addClass('hide');
     });
	socket.on('swipeStart', function(data)
     {
		var i = randomIntFromInterval(0,smoke_array.length-1);
		var clip = smoke_array[i];
		clip.alpha-=0.05;
		checkSmoke()
     });

	socket.on('swipeEnd', function(data)
     {
     	if(swipe_success==0){
			for(var i=0;i<smoke_array.length;i++){
				smoke_array[i].alpha=1;
			}
		}
     });
});

function checkSmoke(){
	var count = 0;
	for(var i=0;i<smoke_array.length;i++){
		if(smoke_array[i].alpha<=0){
			count+=1;
		}
	}
	if(count>=smoke_array.length-1){
		swipe_success=1;
		socket.emit("swipeSuccess", {"gameCode":gameCode});
		var room_scene = stage.getChildByName('room');
     	var scale = 1;
		var posX = 0;
		var posY = 0;
		createjs.Tween.get(room_scene,{paused:false}).to({scaleX:scale,scaleY:scale,x:posX,y:posY},2000).call(function(){
			$('#swipe-instruction2').removeClass('hide');
		});
	}
}

function hideDoorScene(){
	var door_scene = stage.getChildByName('door');
	var room_scene = stage.getChildByName('room');
	console.log(door_scene);
		createjs.Tween.get(door_scene,{paused:false}).to({alpha:1}).wait(4000);
		createjs.Tween.get(door_scene,{override:true}).play(
			createjs.Tween.get(door_scene,{paused:false})
				.to({alpha:0},1000)
				.call(function(){
					room_scene.visible=true;
					room_scene.alpha=1;
					setTimeout(function(){
					//	setPushState('living room','room');
					},400)
				})
		);
}