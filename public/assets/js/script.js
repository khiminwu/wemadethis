var current_state ="home";
var last_state = "";
var canvas,stage,w,h;

var socket;

$(document).ready(function(){

	canvas = document.getElementById("canvas");
	stage = new createjs.Stage(canvas);
	


	socket = io.connect(base_url);
	
	socket.emit("device", {"type":"game", "gameCode":gameCode});

	socket.on('getPage', function(data)
     {
     	  $('#content-area').html(data);
     });

	socket.on('connectionLost', function(data)
     {
     	  setPushState('home','home')
     	  resetCanvas();
     });

	

	$(window).bind("popstate", function(e){
		console.log(e.currentTarget)
		resetCanvas();
	});
	
	$(window).resize(function() {
		windowResize()
	});

	/*loadPage();

	windowResize();*/
	windowResize();

	socket.on('connected', function(data){
		//console.log('connected');
		setPushState('house','house');
    });

    socket.on('showNextPage', function(data){
		setPushState(data.page,data.page);
    });



    
	
	createjs.Ticker.addListener(window);
    createjs.Ticker.setFPS(50);

});

function tick() {
    stage.update();
}

function loadPage(){
	
	var str = location.href;
	str = str.replace(base_url,'')
	str = str.split('/');
	current_state = str[1];
	if(current_state==""){
		current_state = 'home';
	}
	$.ajax({
	  type: "POST",
	  url: base_url+'/'+current_state,
	  data: {device:gameCode},
	  success: function(data){
	  	if(data=="false"){
	  		setPushState('home','')
	  	}else{
	  		$('#content-area').html(data);
	  	}
	  	
	  }
	});
	
}

function setPushState(title,url)
{
	
	history.pushState('',title,base_url+'/'+url);
	last_state = current_state;
	current_state = url;	
	loadPage()
}

function windowResize(){
	w = $('#canvas-area').width();
	h = $('#canvas-area').height();
	$(canvas).width(w);
	$(canvas).height('auto');
	if($(canvas).height()<h){
		$(canvas).width('auto');
		$(canvas).height(h);
	}
	var posY = 0-(($(canvas).height()-h)/2);
	var posX = 0-(($(canvas).width()-w)/2);
	$(canvas).css({'top':posY,'left':posX});
}