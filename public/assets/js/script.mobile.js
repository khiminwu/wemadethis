var socket;
var countdown=0;
var page="";

$(document).ready(function(){
	socket = io.connect(base_url);
	socket.on('welcome', function(data)
     {
        socket.emit("device", {"type":"controller", "gameCode":gameCode});
     });
	  socket.on("connected", function(data)
	  {
	     $('#start-btn').removeClass('hide');
	     $('#start-btn').show();
	  });

	  socket.on("showNextBtn",function(data){
	  		$('#loading-area').addClass('hide');
	  		$('#btn-area').removeClass('hide');
	  		page = data.page;
	  })

	  socket.on("countdown", function(data)
	  {
	  	$('#result-area').hide();
	  	$('#countdown-area').show();
	  	$('#countdown-area span').html('');
		TweenMax.to($('#countdown-area span'), 0, {scaleX:1,scaleY:1,opacity:1,ease:Strong.easeOut})
	  	$('#countdown-area span').html(data.count);
	  	TweenMax.to($('#countdown-area span'), 1, {scaleX:2,scaleY:2,opacity:0,ease:Quart.easeIn})
	  });

	  socket.on("startPlaying", function(data)
	  {
	  	$('#countdown-area').hide();
	  	$('#play-area').show();
	  });

	  socket.on("knockArea", function(data)
	  {
	  	$('#loading-area').removeClass('hide');
	  	setTimeout(function(){
	  		$('#knock-area').removeClass('hide');
	  		$('#loading-area').addClass('hide');
	  	},3000)
	  });


	  socket.on("roomArea",function(data){
	  	$('#room-area').removeClass('hide');
	  	$('#loading-area').addClass('hide');
	  })




	  socket.on("gameOver", function(data)
	  {
	  	$('#result-area').show();
	  	$('#play-area').hide();

	  	$('#gameover').show();
	  	$('#result').hide();
	  	$('#score-total').html(data.score*100);
	  	setScore();
	  	
	  });

	  socket.on("fail", function(data)
	  {
	  			
				
	  });

	  socket.on('connectionLost', function(data)
	     {
	     		$('#btn-area').addClass('hide');
	     		$('#loading-area').addClass('hide');
	     		$('#instruction-area').addClass('hide');
	     		$('#knock-area').addClass('hide');
	     		$('#room-area').addClass('hide');

	     		$('#error-area').removeClass('hide');
	     });

	$(window).resize(function(){
		resize();
	})
	$("#play-area").swipe( {
	    //Generic swipe handler for all directions
	    swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
	    	//alert(direction)
	    	socket.emit("swipe", {"gameCode":gameCode,"direction":direction});
	    }
	  });
	resize();
});


function resize(){
	
}

function nextPage(){
	socket.emit("showNextPage", {"page":page, "gameCode":gameCode});
	$('#btn-area').addClass('hide');
}

function startGame(){
	socket.emit("startGame", {"gameCode":gameCode});
}

function startJourney(){
	socket.emit("startJourney", {"gameCode":gameCode});	
	$('#landing-area').addClass('hide');
	$('#loading-area').removeClass('hide');
}
