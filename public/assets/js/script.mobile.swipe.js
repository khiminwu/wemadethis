

$(document).ready(function(){
	$("#swipe-area").swipe( {
        swipeStatus:function(event, phase, direction, distance, duration)
        {
          var str = "";
          if (phase=="move")
            socket.emit("swipeStart", {"gameCode":gameCode});
          if (phase=="end")
            socket.emit("swipeEnd", {"gameCode":gameCode});
          },
        triggerOnTouchLeave:true,
        threshold:null
      });
	//statusSwipe("ready");
});



function statusSwipe(stat) {
  $("#room-area.game-area").removeClass("ready");
  $("#room-area.game-area").removeClass("success");
  $("#room-area.game-area").removeClass("error");
  $("#room-area.game-area").addClass(stat);
  $('#room-area.ready').addClass('hide');
  $('#room-area.error').addClass('hide');
  $('#room-area.success').addClass('hide');
  $('#room-area.'+stat).removeClass('hide');
}


function swipePlay(){
	$('#room-area .instruction-area').addClass('hide');
	$('#room-area .play-area').removeClass('hide');
  socket.emit("startSwipeGame", {"gameCode":gameCode});
  setTimeout(function(){
    statusSwipe("ready");
  },2500)
}