var socket;
var countdown=0;

$(document).ready(function(){
	$('#start-btn').hide();
	$('#play-area').hide();  
	$('#countdown-area').hide();
	$('#result-area').hide();
	$('#disconnect-area').hide();
	$('#room-area').hide();
	socket = io.connect(base_url);
	socket.on('welcome', function(data)
     {
        // Send 'controller' device type with our entered game code
        socket.emit("device", {"type":"controller", "gameCode":gameCode});
     });
	  socket.on("connected", function(data)
	  {
	     $('#start-btn').removeClass('hide');
	     $('#start-btn').show();
	  });

	  socket.on("countdown", function(data)
	  {
	  	$('#result-area').hide();
	  	$('#countdown-area').show();
	  	$('#countdown-area span').html('');
		TweenMax.to($('#countdown-area span'), 0, {scaleX:1,scaleY:1,opacity:1,ease:Strong.easeOut})
	  	$('#countdown-area span').html(data.count);
	  	TweenMax.to($('#countdown-area span'), 1, {scaleX:2,scaleY:2,opacity:0,ease:Quart.easeIn})
	  });

	  socket.on("startPlaying", function(data)
	  {
	  	$('#countdown-area').hide();
	  	$('#play-area').show();
	  });


	  socket.on("gameOver", function(data)
	  {
	  	$('#result-area').show();
	  	$('#play-area').hide();

	  	$('#gameover').show();
	  	$('#result').hide();
	  	$('#score-total').html(data.score*100);
	  	setScore();
	  	
	  });

	  socket.on("fail", function(data)
	  {
	  			$('#instruction-area').hide();
			  	$('#start-btn').hide();
				$('#play-area').hide();  
				$('#countdown-area').hide();
				$('#result-area').hide();
				$('#disconnect-area').hide();
				$('#room-area').show();
				
	  });

	  socket.on('connectionLost', function(data)
	     {
	     		$('#instruction-area').hide();
			  	$('#start-btn').hide();
				$('#play-area').hide();  
				$('#countdown-area').hide();
				$('#result-area').hide();
				$('#room-area').hide();
				$('#disconnect-area').show();
	     });

	$(window).resize(function(){
		resize();
	})
	$("#play-area").swipe( {
	    //Generic swipe handler for all directions
	    swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
	    	//alert(direction)
	    	socket.emit("swipe", {"gameCode":gameCode,"direction":direction});
	    }
	  });
	resize();
});

function setScore(){
	setTimeout(function(){
  		
  		$('#gameover').hide();
  		$('#result').show();
  	},1000)
}

function resize(){
	
}

function startGame(){
	socket.emit("startGame", {"gameCode":gameCode});
	$('#countdown-area').show();
    $('#instruction-area').hide();
}
