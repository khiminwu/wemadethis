var socket;
var countdown=0;

$(document).ready(function(){
	$('#start-btn').hide();
	$('#play-area').hide();  
	$('#countdown-area').hide();
	$('#result-area').hide();
	$('#disconnect-area').hide();
	$('#room-area').hide();
	$('#info-area').hide();
	$('#gyro-area').hide();

	
	setDeviceMotion()

	socket = io.connect(base_url);
	socket.on('welcome', function(data)
     {
        // Send 'controller' device type with our entered game code
        socket.emit("device", {"type":"controller", "gameCode":gameCode});
     });
	  socket.on("connected", function(data)
	  {
	     $('#start-btn').removeClass('hide');
	     $('#start-btn').show();
	  });

	  socket.on("countdown", function(data)
	  {
	  	$('#result-area').hide();
	  	$('#countdown-area').show();
	  	$('#countdown-area span').html('');
		TweenMax.to($('#countdown-area span'), 0, {scaleX:1,scaleY:1,opacity:1,ease:Strong.easeOut})
	  	$('#countdown-area span').html(data.count);
	  	TweenMax.to($('#countdown-area span'), 1, {scaleX:2,scaleY:2,opacity:0,ease:Quart.easeIn})
	  });

	  socket.on("startPlaying", function(data)
	  {
	  	 $('#gyro-area').show();	
	  	 setDeviceMotion();
	  });


	  socket.on("gameOver", function(data)
	  {
	  	
	  });

	  socket.on("fail", function(data)
	  {
				
	  });

	  socket.on('connectionLost', function(data)
	  {
	     	
	  });

	$(window).resize(function(){
		resize();
	})
	resize();
});

function setDeviceMotion(){
	//alert(window.DeviceMotionEvent);
	if (window.DeviceMotionEvent) {
		$('#start-btn').show();
	 	window.addEventListener('deviceorientation', function(eventData){
	 		// gamma is the left-to-right tilt in degrees, where right is positive
		    var tiltLR = Math.ceil(eventData.gamma);

		    // beta is the front-to-back tilt in degrees, where front is positive
		    var tiltFB = Math.ceil(eventData.beta);

		    // alpha is the compass direction the device is facing in degrees
		    var dir = Math.ceil(eventData.alpha);
		    //alert()
		    // call our orientation event handler
		    deviceOrientationHandler(tiltLR, tiltFB, dir);
	 	}, false);
	} else {
	 	$("#error-area").html("Not supported.");
	}
}

function deviceOrientationHandler(tiltLR,tiltFB,dir) {
	$('#doTiltLR').html(tiltLR);
	$('#doTiltFB').html(tiltFB);
	$('#doDirection').html(dir);
	var orientation = new object();
	orientation['tiltLR'] = tiltLR;
	orientation['tiltFB'] = tiltFB;
	orientation['dir'] = dir;
	socket.emit("gyroControl", orientation);
      
}

function setScore(){
	setTimeout(function(){
  		
  		$('#gameover').hide();
  		$('#result').show();
  	},1000)
}

function resize(){
	
}

function startGame(){
	//alert(socket);
	socket.emit("startGame", {"gameCode":gameCode});
	//setDeviceMotion();
    $('#instruction-area').hide();
}


