
var an = '';
var score = 0;
var countdown=0;
var count;
var timer=30000;
var socket;
$(document).ready(function(){
	socket = io.connect(base_url);
	$('#instruction-area').show();
	$('#question-area').hide();
	$('#result-area').hide();
	socket.on('welcome', function(data){
		console.log(gameCode);
        // Send 'controller' device type with our entered game code
    	socket.emit("device", {"type":"game", "gameCode":gameCode});    
     });

	socket.on('playing', function(data)
     {
     	  countdown=3;
          $('#instruction-area').hide();
		  $('#question-area').show();
		  $('#result-area').hide();
		  $('#timer').html('')
		  setCountdown();
		  score=0;
		  $.each($('.question'),function(key,val){
				$(this).addClass('hide');
			}) 
		  //generateQuestion();
     });

	socket.on('checkDirection', function(data)
     {
		  checkAnswer(data.direction);
     });

	socket.on('connectionLost', function(data)
     {
     	console.log('disconnect');
		  	$('#instruction-area').show();
			$('#question-area').hide();
			$('#result-area').hide();
			clearInterval(count);
     });

	
	$(window).resize(function(){
		resize();
	})
	resize();
});

function setCountdown(){
	$('#countdown-area').html('');

	TweenMax.to($('#countdown-area'), 0, {scaleX:1,scaleY:1,opacity:1,ease:Strong.easeOut})
	if(countdown>-1){
		var count_str = countdown;
		if(countdown==0){
			count_str = 'start';
		}
		$('#countdown-area').html(count_str);
		$('#countdown-area').css('opacity',1);
		countdown-=1;
		TweenMax.to($('#countdown-area'), 1, {scaleX:2,scaleY:2,opacity:0,ease:Quart.easeIn,onComplete:setCountdown})
		socket.emit("countdown", {"count":count_str, "gameCode":gameCode}); 
	}else{
		setTimer();
		generateQuestion();
		socket.emit("startPlaying", {"gameCode":gameCode});
	}
}

function setTimer(){
	$('#timer').html(timer/1000)
	var counter = 0;
	count = setInterval(function(){
		if(counter<=(timer/1000)+1){
			var counter_str = (timer/1000)-counter;
			if((timer/1000)-counter==-1){
				counter_str="Time's up";
				socket.emit("gameOver", {score:score,"gameCode":gameCode});
			}
			$('#timer').html(counter_str)
		}else{
			clearInterval(count);
			$('#question-area').hide();
			$('#result-area').show();
			$('#score-total').html(score*100);
		}
		counter+=1;
	},1000)
}

function resize(){
	var h = $(window).height();
	$.each($('#controller li'),function(){
		$(this).height(h/4);
	})
}

function checkAnswer(val){
	var tl = new TimelineMax({delay:0.3,onComplete:generateQuestion});
	if(val==an){
		score+=1;
		tl.to("body,html", 0, {backgroundColor:"#6fd065"})
		  .to("body,html", .5, {backgroundColor:"#eeeded"});
		//TweenMax.to("body,html", 0, {backgroundColor:"#6fd065"}).to("body,html", .5, {backgroundColor:"#B4B4B4",delay:.5,onComplete: generateQuestion()})
	}else{
		tl.to("body,html", 0, {backgroundColor:"#a83e3e"})
		  .to("body,html", .5, {backgroundColor:"#eeeded"});
		//TweenMax.to("body,html", 0, {backgroundColor:"#a83e3e"}).to("body,html", .5, {backgroundColor:"#B4B4B4",delay:.5,onComplete: generateQuestion()})
	}
}

function generateQuestion(){
	var qn = randomIntFromInterval(0,7);
	$.each($('.question'),function(key,val){
		if(key==qn){
			$(this).removeClass('hide');
			TweenMax.from($(this), 0.3, {scaleX:1.5,scaleY:1.5,ease:Back.easeOut})
			an = $(this).data('an');
		}else{
			$(this).addClass('hide');
		}
	}) 
}



function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}