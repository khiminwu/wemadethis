$(document).ready(function(){
	var home_scene = stage.getChildByName('home');
	var door_scene = stage.getChildByName('door');
	var scale = 1.5;
	var posX = 0-((w*scale)-w)/2;
	var posY = 0-((h*scale)-h)/2;
		createjs.Tween.get(door_scene,{paused:false}).to({alpha:1}).wait(4000);
		createjs.Tween.get(home_scene,{override:true}).play(
			createjs.Tween.get(home_scene,{paused:false})
				.to({scaleX:scale,scaleY:scale,x:posX,y:posY},4000)
				.to({alpha:0},1000, createjs.Ease.quartIn)
				.call(function(){
					socket.emit("showNextBtn",{"page":"door", "gameCode":gameCode})
				})
		);
	
	
	/*$(window).resize(function(){
		resize();
	})
	resize();*/
});
