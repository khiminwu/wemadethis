
var an = '';
var score = 0;
var countdown=0;
var count;
var timer=30000;
var socket;
$(document).ready(function(){
	socket = io.connect(base_url);
	$('#instruction-area').show();
	$('#playing-area').hide();
	socket.on('welcome', function(data){
		console.log(gameCode);
        // Send 'controller' device type with our entered game code
    	socket.emit("device", {"type":"game", "gameCode":gameCode});    
     });

	socket.on('playing', function(data)
     {
     	  countdown=3;
          $('#instruction-area').hide();
		  $('#playing-area').show();
		  setCountdown();
		  score=0;
		 
     });

	socket.on('checkDirection', function(data)
     {
		  checkAnswer(data.direction);
     });

	socket.on('connectionLost', function(data)
     {
     	console.log('disconnect');
		$('#instruction-area').show();
		$('#question-area').hide();
		$('#result-area').hide();
     });

	
	$(window).resize(function(){
		resize();
	})
	resize();
});

function setCountdown(){
	$('#countdown-area').html('');

	TweenMax.to($('#countdown-area'), 0, {scaleX:1,scaleY:1,opacity:1,ease:Strong.easeOut})
	if(countdown>-1){
		var count_str = countdown;
		if(countdown==0){
			count_str = 'start';
		}
		$('#countdown-area').html(count_str);
		$('#countdown-area').css('opacity',1);
		countdown-=1;
		TweenMax.to($('#countdown-area'), 1, {scaleX:2,scaleY:2,opacity:0,ease:Quart.easeIn,onComplete:setCountdown})
		socket.emit("countdown", {"count":count_str, "gameCode":gameCode}); 
	}else{
		socket.emit("startPlaying", {"gameCode":gameCode});
	}
}


function resize(){
	var h = $(window).height();
	$.each($('#controller li'),function(){
		$(this).height(h/4);
	})
}


function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

