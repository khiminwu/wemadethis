$(document).ready(function(){
	var house_scene = stage.getChildByName('house');
	var door_scene = stage.getChildByName('door');
	door_scene.alpha=0;

	var scale = 1.5;
	var posX = 0-((w*scale)-w)/2;
	var posY = 0-((h*scale)-h)/2;
		createjs.Tween.get(door_scene,{paused:false}).to({alpha:1}).wait(3000);
		createjs.Tween.get(house_scene,{override:true}).play(
			createjs.Tween.get(house_scene,{paused:false})
				.to({scaleX:scale,scaleY:scale,x:posX,y:posY},4000)
				.to({alpha:0},1000, createjs.Ease.quartIn)
				.call(function(){
					//socket.emit("showNextBtn",{"page":"door", "gameCode":gameCode})
				})
		);

	socket.on('knockSuccess', function(data){
		//console.log('asdasd')
		hideDoorScene()
		//setPushState(data.page,data.page);
    });
});


function hideDoorScene(){
	var door_scene = stage.getChildByName('door');
	var room_scene = stage.getChildByName('room');
		createjs.Tween.get(door_scene,{paused:false}).to({alpha:1}).wait(4000);
		createjs.Tween.get(door_scene,{override:true}).play(
			createjs.Tween.get(door_scene,{paused:false})
				.to({alpha:0},1000)
				.call(function(){
					room_scene.visible=true;
					room_scene.alpha=1;
					setPushState('living room','room')
				})
		);
}

