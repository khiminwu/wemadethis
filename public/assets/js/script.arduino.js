var socket;
$(document).ready(function(){
	socket = io.connect(base_url);

	$('[data-slider]').on('change.fndtn.slider', function(){
		//var color=[$('#red').val(),$('#green').val(),$('#blue').val()];
		socket.emit("changeColor", {"red":$('#red').val(),"green":$('#green').val(),"blue":$('#blue').val()});
	});
});