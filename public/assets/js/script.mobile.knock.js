var tolerance_ms = 500,
    start,
    end,
    clicks = 0, 
    wrong = false, 
    timeout;


var pass = [0, 500, 1000, 1500, 2000];

$(document).ready(function(){
	$("#show").click(function(){ showPassword(); });
	$("#knock-area-play").swipe( {
        tap:function(event, target) {
        	
          var e = {
		    pageX : event.pageX,
		    pageY : event.pageY
		  };
		  //console.log(e)
          click(e);
        },
        threshold:50
      });
	
});


function click(e){
  if(clicks==0) {
    start = new Date();
    wrong = false;
  } else {
    end = new Date();
    if(Math.abs((end-start)-pass[clicks])>tolerance_ms) 
      wrong = true;
  }
  status("ready");
  circle(e); //demo only
  clicks++;
  clearTimeout(timeout);
  timeout = setTimeout(function(){ check(); },1000);
}
  
function check(){
	//console.log(wrong,clicks);
  //check password
  (clicks!=pass.length) ? error() : success();
  clicks = 0;
  $(".circle").remove(); //demo only
}

function success() {
  //success callback
  status("success");
  setTimeout(function(){
  	$('#loading-area').removeClass('hide');
  	$('#knock-area').addClass('hide');
  	socket.emit("knockSuccess", {"gameCode":gameCode});
  },1000)
}
function error() {
  //error callback
  status("error");
}

function status(stat) {
  $("#knock-area.game-area").removeClass("ready");
  $("#knock-area.game-area").removeClass("success");
  $("#knock-area.game-area").removeClass("error");
  $("#knock-area.game-area").addClass(stat);
  $('#knock-area.ready').addClass('hide');
  $('#knock-area.error').addClass('hide');
  $('#knock-area.success').addClass('hide');
  $('#knock-area.'+stat).removeClass('hide');
}



//demo only

function showPassword(){
  status("ready");
  var e = {
    pageX : $(window).width()/2,
    pageY : $(window).height()/2
  };
  //console.log(pass)
  $.each(pass, function(){
    setTimeout(function(){
      circle(e);
    },this)
  });
}



function circle(e) {
  //draw circle: for demo purposes only
  $("#knock-area-play").append(
    $("<div>").addClass("circle").css({
      left : e.pageX,
      top : e.pageY
    })
  );
} 

function knockPlay(){
	$('#knock-area .instruction-area').addClass('hide');
	$('#knock-area .play-area').removeClass('hide');
  status("ready");
}