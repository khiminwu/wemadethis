var interval,queue,preloader,manifest,regX,regY,smoke_array;
$(document).ready(function(){
	preloader = new createjs.Shape();
	preloader.graphics.beginFill("rgba(14,112,181,0.8)").drawRect(0, 0, 1280,1000);
	stage.addChild(preloader);
	preloader.scaleX = 0;
	
	manifest = [
       {id: "landing", src:"assets/img/north-pole.jpg"},
       {id: "northpole", src:"assets/img/north-pole2.jpg"},
       {id: "door", src:"assets/img/north-pole3.jpg"},
       {id: "room", src:"assets/img/room.png"},
       {id: "smoke1", src:"assets/img/smoke01.png"},
       {id: "smoke2", src:"assets/img/smoke02.png"},
       {id: "smoke3", src:"assets/img/smoke03.png"},
       {id: "smoke4", src:"assets/img/smoke04.png"},
       {id: "smoke5", src:"assets/img/smoke05.png"},
       {id: "smoke6", src:"assets/img/smoke06.png"}

    ]

	queue = new createjs.LoadQueue(true);
    queue.on("complete", handleComplete, this);
    queue.on("progress", handleProgress);

    queue.loadManifest(manifest);
});

function handleProgress(event) {
	preloader.scaleX = (event.loaded+1)/manifest.length;
}

function handleComplete() {
	regX = 0-(w/2);
	regY = 0-(h/2);

    preloader.visible=false;

    generateRoom();
    generateDoor();
    generateHouse();
    generateLanding();
    
    

    loadPage();

	windowResize();
}



function generateHouse(){
	var house_scene = new createjs.Container();
	var house = new createjs.Bitmap(queue.getResult("northpole"));
    house.cache(0,0,1280,800);
    house_scene.addChild(house);
    house_scene.house = house;
    house_scene.name="house";
    house_scene.regX = regX;
  	house_scene.regY = regY;
    house.x = house_scene.regX;
    house.y = house_scene.regY;

    house_scene.regX = regX;
  	house_scene.regY = regY;
    stage.addChild(house_scene);
    
}

function generateLanding(){
	var mp = 150; //max particles
	var particles = [];
	var home_scene = new createjs.Container();
	var angle = 0;
	home_scene.name="home";
	
	

	var bg = new createjs.Bitmap(queue.getResult("landing"));
    bg.cache(0,0,1280,800);
    home_scene.addChild(bg);
    home_scene.bg = bg;


    home_scene.regX = regX;
  	home_scene.regY = regY;
    stage.addChild(home_scene);

    bg.x = home_scene.regX;
    bg.y = home_scene.regY;
    
    for(var i = 0; i < mp; i++)
	{
		particles.push({
			x: home_scene.regX + (Math.random()*w), //x-coordinate
			y: home_scene.regY + (Math.random()*h), //y-coordinate
			r: Math.random()*4+1, //radius
			d: Math.random()*mp, //density
			p: new Object()
		})
	}

	

	function draw()
	{
		
		for(var i = 0; i < mp; i++)
		{

			var p = particles[i];
			 var circle = new createjs.Shape();
			 circle.graphics.beginFill("rgba(209,215,255,0.6)").drawCircle(0, 0, p.r);
			 //circle.graphics.beginFill("red").drawCircle(0, 0, p.r);
			 circle.x = p.x;
			 circle.y = p.y;
			 home_scene.addChild(circle);
			 p.p = circle;
		}

		interval = setInterval(update,40);
	}

	function update()
	{
		angle += 0.001;

		for(var i = 0; i < mp; i++)
		{
			var p = particles[i];

			//Updating X and Y coordinates
			//We will add 1 to the cos function to prevent negative values which will lead flakes to move upwards
			//Every particle has its own density which can be used to make the downward movement different for each flake
			//Lets make it more random by adding in the radius
			p.p.y += Math.cos(angle+p.d) + 1 + p.r/2;
			p.p.x += Math.sin(angle) * 2;
			
			//Sending flakes back from the top when it exits
			//Lets make it a bit more organic and let flakes enter from the left and right also.
			if(p.p.x > home_scene.regX+w+5 || p.p.x < home_scene.regX - 5 || p.p.y > h)
			{
				if(i%3 > 0) //66.67% of the flakes
				{
					p.p.x= home_scene.regX + Math.random()*w;
					p.p.y= home_scene.regY-10
				}
				else
				{
					//If the flake is exitting from the right
					if(Math.sin(angle) > 0.1)
					{
						//Enter from the left
						p.p.x= home_scene.regX-5;
						p.p.y= home_scene.regY+Math.random()*h;
					}
					else
					{
						//Enter from the right
						p.p.x= home_scene.regX+(w+5);
						p.p.y= home_scene.regY+Math.random()*h;
					}
				}
			}
		}
	}

	draw();
}

function generateDoor(){
	var door_scene = new createjs.Container();
	door_scene.name="door";
	var bg = new createjs.Bitmap(queue.getResult("door"));
    bg.cache(0,0,1280,800);
    door_scene.addChild(bg);

    door_scene.regX = regX;
  	door_scene.regY = regY;
    stage.addChild(door_scene);

    bg.x = regX;
    bg.y = regY;

}

function generateRoom(){
	var room_scene = new createjs.Container();
	room_scene.name="room";
	var bg = new createjs.Bitmap(queue.getResult("room"));
    bg.cache(0,0,1280,800);
    room_scene.addChild(bg);

    room_scene.regX = regX;
  	room_scene.regY = regY;
    stage.addChild(room_scene);

    bg.x = regX;
    bg.y = regY;

    var smoke_area = new createjs.Container();
    room_scene.smoke=smoke_area;
    room_scene.addChild(smoke_area);

    smoke_array = [];

    for(var i=0;i<6;i++){
    	var smoke = new createjs.Bitmap(queue.getResult("smoke"+(i+1)));
    	smoke_area.addChild(smoke);
    	smoke_array.push(smoke);
    	if(i==0){
    		smoke.x=0;
    		smoke.y=0;
    	}else if(i==1){
    		smoke.x=0;
    		smoke.y=100;
    	}else if(i==2){
    		smoke.x=-50;
    		smoke.y=-50;
    	}else if(i==3){
    		smoke.x=150;
    		smoke.y=-150;
    	}else if(i==4){
    		smoke.x=50;
    		smoke.y=-100;
    	}else if(i==5){
    		smoke.x=200;
    		smoke.y=100;
    	}
    	smoke.defX = smoke.x;
    	smoke.defY = smoke.y;
    }

    smoke_area.x=-200;
    smoke_area.y=-200;

    for(var i=0;i<smoke_array.length;i++){
		smokeMove(smoke_array[i]);
	}

    //setInterval(smokeMove,100)

    function smokeMove(clip){
    	var newX = randomIntFromInterval(clip.defX-10,clip.defX+10);
    	var newY = randomIntFromInterval(clip.defY-10,clip.defY+10);
    	createjs.Tween.get(clip,{paused:false}).to({x:newX,y:newY},2000).call(function(){
    		smokeMove(clip);
    	})
    }


}

function resetCanvas(){
	console.log('reset')
	for(var i=0;i<stage.getNumChildren();i++){
		var mc = stage.getChildAt(i);
		mc.visible=false;
		console.log(mc.name)
		if(mc.name=='home'){
			mc.bg.alpha=1;
			mc.visible=true;
			mc.alpha=1;
			mc.scaleX=1;
			mc.scaleY=1;
			mc.x=0;
			mc.y=0;
		}
	}
	
}

function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}





