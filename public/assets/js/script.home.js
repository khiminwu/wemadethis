var canvas = document.getElementById("canvas"),
	context = canvas.getContext("2d"),
	video = document.getElementById("video"),
	videoObj = { "video": true },
	errBack = function(error) {
		console.log("Video capture error: ", error.code); 
	};
var localStream;
var guide,canvas,raster,head_group;
var w = canvas.width;
var h = canvas.height;

paper.install(window);

$(document).ready(function(){

	$('#crop-btn').hide();


	$('#snap').click(function(){
		
		raster = new Raster(draw());
    	raster.position=new Point(w/2,h/2);
    	//raster.rasterize();
		/*raster = new Raster('sample');
    	raster.position=new Point(320,240);	*/
    	raster.sendToBack();
    	if(localStream){
	    	localStream.stop();
	    }
	    $('#video').hide();	
    	$('#crop-btn').show();
    	$('#snap').hide();
	})


	
	
	// Put video listeners into place
	if(navigator.getUserMedia) { // Standard
		navigator.getUserMedia(videoObj, function(stream) {
			video.src = stream;
			video.play();
			localStream = stream;
		}, errBack);
	} else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
		navigator.webkitGetUserMedia(videoObj, function(stream){
			video.src = window.webkitURL.createObjectURL(stream);
			video.play();
			localStream = stream;
		}, errBack);
	}
	else if(navigator.mozGetUserMedia) { // Firefox-prefixed
		navigator.mozGetUserMedia(videoObj, function(stream){
			video.src = window.URL.createObjectURL(stream);
			video.play();
			localStream = stream;
		}, errBack);
	}

	

	
	var hitOptions = {
        segments: true,
        stroke: true,
        fill: true,
        tolerance: 5
    };
    var segment, path;
	var tool = new Tool();
	//tool.distanceThreshold = 100;
	canvas = document.getElementById('canvas');
	
		// Create an empty project and a view for the canvas:

	paper.setup(canvas);
    /*raster = new Raster('sample');
    raster.position=new Point(320,240);*/
    guide = createGuide(0, 300, 20);
    guide.scale(0.9,1.2);
    var lightness = (Math.random() - 0.5) * 0.4 + 0.4;
    var hue = Math.random() * 360;
    //path.fillColor = { hue: hue, saturation: 1, lightness: lightness };
    guide.fillColor = 'rgba(255,0,0,0.05)';
    guide.strokeColor = 'rgba(255,0,0,0.5)';
    guide.strokeWidth = 2;
    guide.dashArray = [10, 12];
    guide.strokeCap = 'round';
    resizeCanvas();
	paper.view.draw();


	tool.onMouseMove =function(event) {
			var hitResult = project.hitTest(event.point, hitOptions);
			project.activeLayer.selected = false;
			//if(hitResult!=null && hitResult.item!=raster){
	            
	            if (hitResult && hitResult.item)
	                hitResult.item.selected = true;
	      //  }
    }



    tool.onMouseDown = function(event) {
    	segment = path = null;
    	var hitResult = project.hitTest(event.point, hitOptions);        
        if (event.modifiers.shift) {
            if (hitResult.type == 'segment') {
                hitResult.segment.remove();
            };
            return;
        }

        if (hitResult) {
            path = hitResult.item;
            if(path!=raster){
	            if (hitResult.type == 'segment') {
	                segment = hitResult.segment;
	            } else if (hitResult.type == 'stroke') {
	                var location = hitResult.location;
	                segment = path.insert(location.index + 1, event.point);
	                
	            }
	            hitResult.item.bringToFront();
	        }
        }
    }

    tool.onMouseDrag =function(event) {

        if (segment) {
        	console.log(event);
            segment.point = event.point;
            
        } else if (path) {
            //path.position += event.delta;
        }
    }

    $(window).resize(function(){
		resizeCanvas();
	});

    $('#crop-btn').click(function(){
    	head_group = new Group(guide, raster);
    	head_group.clipped = true;
    	downloadAsSVG('pic01');
    });
    

})

function draw() {
	console.log(w,h)
   var canvas2 = document.getElementById('canvas-temp');
   var context2 = canvas2.getContext('2d');
   

   if(video.paused || video.ended) return false; // if no video, exit here

   context2.drawImage(video,0,0,w,h); // draw video feed to canvas
   
   var uri = canvas2.toDataURL("image/png"); // convert canvas to data URI
   
   return uri; // add URI to IMG tag src
}


//currently name doesn't seem to work in some browsers.
//Save SVG from paper.js as a file.
function downloadAsSVG (fileName) {

   if(!fileName) {
       fileName = "sample.png"
   }
   var tempImg = head_group.rasterize();
   var rect = setRect()
   canvasSrc = tempImg.getSubCanvas(rect)
   canvasSrc = new Raster(canvasSrc);
   
   var svg = canvasSrc.toDataURL("image/png").replace(/^data:image\/(png|jpg);base64,/, "");  
   svg = JSON.stringify(svg);
   canvasSrc.remove();
   tempImg.remove();
   ///console.log(svg);
   var post_url = '/image/create';
   $.ajax({
		type: "POST",
		url: post_url,
		data: {image:svg,filename:fileName},
		success: function(data){
			console.log(data);
		}
	});
   
}

function setRect(){
	var minX=maxX=minY=maxY=-1
	$.each(guide.segments,function(key,val){
	  if(minX<0){
	    minX=val.point.x;
	  }else if(minX>val.point.x){
	    minX=val.point.x;
	  }
	  
	  if(maxX<0){
	    maxX=val.point.x;
	  }else if(maxX<val.point.x){
	    maxX=val.point.x;
	  }
	  
	  if(minY<0){
	    minY=val.point.y;
	  }else if(minY>val.point.y){
	    minY=val.point.y;
	  }
	  
	  if(maxY<0){
	    maxY=val.point.y;
	  }else if(maxY<val.point.y){
	    maxY=val.point.y;
	  }
	 // console.log(val.point.y)
	});
	var width = Math.abs(minX-maxX);
	var height = Math.abs(minY-maxY);
	var x = guide.position.x;
   	var y = guide.position.y;

   	return new Rectangle(x-(width/2),y-(height/2),width,height);
}

function resizeCanvas(){
	var w = $('#canvas').width();
	guide.position = new Point((w/2), 200);
}

function createGuide(center, maxRadius, points) {
   var path = new Path();
    path.closed = true;
    for (var i = 0; i < points; i++) {
        var delta = new Point({
        	center: [0, 0],
            length: (maxRadius * 0.5),
            angle: (360 / points) * i
        });
        path.add(delta);
    }
    
   

    return path;
}



